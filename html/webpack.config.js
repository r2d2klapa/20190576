const path = require("path");

module.exports = {
    // el archivo de entrada para la compilación
    entry: {
        bundle: "./src/index.js",
        "bundle-cibertec": "./src/index-cibertec.js"
    },
    output: {
        filename: "[name].js",
        // n esta ruta va a colocar el archivo compilado
        path: path.resolve(__dirname, "dist")
    },
    module: {
        rules: [
            // la regla para procesar archivos scss
            {
                test: /\.(scss)$/, // regex que selecciona todos los archivos con extensión ".scss"
                use: [
                    {
                        // para agregar los estilos al DOM del html
                        loader: "style-loader"
                    },
                    {
                        // para poder utilizar los @import dentro de los archivos scss
                        loader: "css-loader"
                    },
                    {
                        // para compilar los archivos sass
                        loader: "sass-loader"
                    }
                ]
            }
        ]
    }
}