import $ from "jquery";

// importar el archivo scss para que sea compilado por webpack
import "./styles/app.scss";

$(function () {
    console.log("Hola desde jQuery!!");

    // obtener el elemento donde queremos agregar el mensaje
    var elemento = $("#app");
    console.log("El elemento es", elemento);

    // agregar html al elemento
    // elemento.html("Hola desde <b>jQuery</b>");
    // elemento.text("Hola desde <b>jQuery</b> Text");

    // enlazar el evento submit al formulario
    $("#form-consulta").on("submit", function (e) {
        // indica que cuando se haga el submit del formulario, no se recargue la página
        e.preventDefault();

        // obtener el valor del código que se desea consultar
        let codigo = e.target["txt-codigo"].value;

        // si el valor de codigo está vacío
        if (!codigo) {
            alert("Ingresa un código válido");
            return;
        }

        // validar que sea número válido
        // isNaN = is not a number
        // if (isNaN(codigo)) {
        //     alert("Ingresa un número válido");
        // }
        
        // solo es válido un # de 3 cifras
        if (!/^[0-9]{2}$/.test(codigo)) {
            alert("Ingresa un número de dos cifras");
            return;
        }

        console.log("codigo ingresado", codigo);

        $.ajax({
            method: "GET",
            url: `https://swapi.co/api/people/${codigo}`            
        })
        .done(function(data){
            console.log(data);
            // armar el html que vamos a incrustar en el control <ul>
            const html = `<li>${data.name}</li>`; // esto es texto

            // obtener el control con jQuery
            let ul = $("#resultado"); // -> <ul></ul>

            // limpiar la lista <ul> (jQuery)
            ul.html(""); // -> <ul></ul>
            // concatenar el html deseado (jQuery)
            ul.append(html); // -> <ul><li>nombre</li></ul>
        })
    })
})
