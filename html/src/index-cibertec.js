import $ from "jquery";
import "./styles/app.scss";

const URL_BASE = "https://localhost:5001";

$(function () {
    // enlazar el evento onSubmit del formulario
    $("#form-insert").on("submit", function (e) {
        // cancelar el comportamiento por default que tiene el submit
        e.preventDefault();

        // obtener el valor del nombre del producto
        const productName = e.target["productName"].value;

        // hacer el POST al servidor
        $.ajax({
            method: "POST",
            url: `${URL_BASE}/products`,
            // parsear el objeto a string
            data: JSON.stringify({ productName }),
            // definir los headers de la solicitud
            headers: { "Content-Type": "application/json" }
        })
            .done(function (data) {
                // la invoación al servicio ha culminado
                alert("Se insertó el producto")
            })
    })

    // al cargar la página, vamos a llamar al servicio web
    // GET https://localhost:{puerto}/products
    // mediante jquery ajax
    $.ajax({
        method: "GET",
        url: `${URL_BASE}/products`
    }).done(function (data) {
        console.log(data);
        let htmlArray = [];

        // recorrer la data y generar el html para cada fila
        for (const product of data) {
            htmlArray.push(`<tr>
                                <td>${product.productId}</td>
                                <td>${product.productName}</td>
                            </tr>`);
        }

        // obtener el tbody de la tabla-productos y vamos a insertar el html
        $("#tabla-productos tbody").html(htmlArray);
    })
});