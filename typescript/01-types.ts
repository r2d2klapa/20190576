// ejemplos de los tipos en TS
let esBool: boolean = true;
let cadena: string = "123";
let numero: number = 123;
let cualquiera: any = 123;
cualquiera = "123";

// para fechas
let fechaISO = new Date().toISOString();
console.log(fechaISO);

// arreglos
let arreglo = [1, 2, 3, "a", true]; // areglo sin tipos

let arregloNumerico: number[] = [1, 2, 3, 4];

let arregloCualquierTipo: any[] = [1, 2, 3, "a", true]; // areglo sin tipos

// funciones
function sumar(x: number, y: number): number {
    return x + y;
}