interface IProduct {
    id: number;
    productName: string;
    publicar(): void;
    stock?: number;
}

const objeto: IProduct = {
    id: 123,
    productName: "132",
    publicar: function () {
        console.log("publicar");
    },
}

objeto.publicar();