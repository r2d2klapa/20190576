// ejemplos de los tipos en TS
var esBool = true;
var cadena = "123";
var numero = 123;
var cualquiera = 123;
cualquiera = "123";
// para fechas
var fechaISO = new Date().toISOString();
console.log(fechaISO);
// arreglos
var arreglo = [1, 2, 3, "a", true]; // areglo sin tipos
var arregloNumerico = [1, 2, 3, 4];
var arregloCualquierTipo = [1, 2, 3, "a", true]; // areglo sin tipos
