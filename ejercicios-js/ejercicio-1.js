var entero = 0;
var cadena = "cadena";
var bool = true;
var objeto = { a: 2 };
var arreglo = [4, 3, 2, 1];

console.log(typeof entero);
console.log(typeof cadena);
console.log(typeof bool);
console.log(typeof objeto);
console.log(typeof arreglo);

// uso de let y const
const pi = 3.14;
// pi = 5; esto da error
console.log(pi);

const arregloUnico = [1, 2];
arregloUnico[0] = 10
console.log(arregloUnico);

function funcion() {
    let j = 0
    let i = 0
    console.log(j)
}

funcion()

// funciones

let variableFuncion = function (llave) {
    return llave + 1;
}

let resultado = variableFuncion(11);

console.log(resultado);

function funcion2(numero) {
    console.log(numero);
}

funcion2(123.22);

let funcionFlecha = (nombre) => {
    // las funciones flecha, pueden acceder al contexto que se encuentra fuera de la misma
    console.log(cadena + " " + nombre);
    console.log(`valor 1: ${cadena}, valor 2: ${nombre}`)
}

funcionFlecha("otra cadena");

// igualdad
console.log(2 == 2);
console.log(2 == "2");

// igualdad completa
console.log(2 === "2");
console.log("2" !== "2");

// objetos
var carro = {
    marca: "toyota",
    anioFabricacion: 2010,
    tocarClaxon: function () {
        console.log("claxon");
    }
}

console.log(`El año de fabricación es: ${carro.anioFabricacion}`)

carro.tocarClaxon();
